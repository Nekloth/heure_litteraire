# Install dependencies

In the current folder, enter ```npm install``` to install the required dependencies


# Usage

As of now, just enter ```node app.js```, and check the ```/output``` folder content

# Template information

To make it simple, the template html will be loaded locally, without any webserver.

To display the appropriate quote, the called (the ```app.js``` script) has to pass parameter in queryString.

There are 2 parameters:
- ```time_to_read```: format HHMM (eg. ```0000```, ```1345``` or ```2312```)
- ```version```: the index of the quote, if there are several, starting from ```0```. If only one quote exists, do send ```version=0```

So, final URL looks like: ```index.html?time_to_read=0015&version=0``` for the first quote of quarter past midnight, or ```index.html?time_to_read=1300&version=3``` for the 4th quote of 1pm.