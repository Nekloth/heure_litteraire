# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- ```package.json``` file to initiate the project
- ```output``` folder to receive all images results (to be confirmed)
- ```template``` folder to receive html file template (to be confirmed)
- Background image for the extract and the Title/Author/Translator
- jQuery integration for DOM manipulation
- ```quotes_full_list.js```, to get all the quotes at once. TO BE MANAGED by the data-generator
- Parse the queryString to read the appropriate value: ```index.html?time_to_read=0015&version=0``` (cf usage details in Readme)
- Introduced ExpressJS for a lovcal webserver, easier for the html page

## Changed
- Optimization of the puppeteer object: instancied once for all! Much better response time!