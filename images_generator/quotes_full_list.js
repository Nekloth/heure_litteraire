var quotes  = {
  "0000": [
    {
      "extrait" : "Mais... Je veux dire que le docteur Koerner ne s'est jamais trompé, et que si, par exemple, il avait prédit la mot de l'un de nous... pour minuit sonnant, je serai inquiet... car minuit va sonner... car minuit sonne !",
      "titre" : "L'ange de Minuit",
      "auteur" : "Théodore Barrières et Edouard Plouvier",
      "traducteur" : null,
      "motClef" : "minuit"
    },
    {
      "extrait" : "À minuit, Adamsberg frappait à la porte du Dr Romain. À minuit cinq, le médecin lui ouvrait enfin,  blême et ébouriffé.",
      "titre" : "Dans les bois éternels",
      "auteur" : "Fred Vargas",
      "traducteur" : null,
      "motClef" : "minuit"
    },
    {
      "extrait" : "Je me suis agenouillée au milieu des ancolies du parc, à minuit. J’ai pris dans chaque main une tige et j’ai attendu la réponse.",
      "titre" : "La boite à malice",
      "auteur" : "Terry Brooks",
      "traducteur" : "Frédérique Le Boucher",
      "motClef" : "minuit"
    }

  ],
  "0001": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0002": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0003": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0004": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0005": [
    {
      "extrait" : "À minuit, Adamsberg frappait à la porte du Dr Romain. À minuit cinq, le médecin lui ouvrait enfin,  blême et ébouriffé.",
      "titre" : "Dans les bois éternels",
      "auteur" : "Fred Vargas",
      "traducteur" : null,
      "motClef" : "minuit cinq"
    }
  ],
  "0006": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0007": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0008": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0009": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0010": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0011": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0012": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0013": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0014": [
    {
      "extrait" : "",
      "titre" : "",
      "auteur" : "",
      "traducteur" : null,
      "motClef" : ""
    }
  ],
  "0015": [
    {
      "extrait" : "Il est minuit et quart. Je n'entends pas de bruit dans la maison et le temps passe lentement. {{NEWLINE}}Je suis au lit depuis huit heures et le tic-tac du réveil égrène les minutes qu'il me reste avant de sombrer dans la folie.",
      "titre" : "Et je prendrai tout ce qu'il y a à prendre",
      "auteur" : "Céline Lapertot",
      "traducteur" : null,
      "motClef" : "minuit et quart"
    }
  ]
}