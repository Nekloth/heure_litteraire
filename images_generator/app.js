// https://blog.xebia.fr/2017/11/14/asyncawait-une-meilleure-facon-de-faire-de-lasynchronisme-en-javascript/


const puppeteer = require('puppeteer')
const path = require('path')
const ProgressBar = require('progress')

const express = require('express')
const app = express()


const params = {
  sourceFile : "../data/full_list.json",
  output: {
    folder : "output",
    extension : "png",
    width : 600,
    height: 800
  },
  httpServer : {
    port: 9876
  }
}


var quotes  = require(path.resolve(__dirname, params.sourceFile));


console.log(path.resolve(__dirname, params.sourceFile))

function numberOfQuotes () {
  return new Promise( (resolve, reject) => {
    var c = 0;
    for ( i = 0 ; i < Object.keys(quotes).length ; i++ ) {
      c += quotes[Object.keys(quotes)[i]].length;
    }
    resolve(c);
  });
}


async function generateOne ( page, timeConsidered , versionConsidered , bar ) {
    var quote = { 'time' : timeConsidered, 'version' : versionConsidered};

    var url = `http://localhost:${params.httpServer.port}/images_generator/template/index.html?time_to_read=${quote.time}&version=${quote.version}`
    await page.goto(url);
    await page.setViewport( { "width": params.output.width || 600, "height": params.output.height || 800 });
    await page.screenshot({path: path.resolve(__dirname,'..',params.output.folder,`image_test_${quote.time}_${quote.version}.${params.output.extension}`)});
    bar.tick();

    return `Generated image for ${quote.time}_${quote.version}`;
}




(async () => {   



  var totalQuotes = await numberOfQuotes();
  var bar = new ProgressBar('[:bar] :percent - Remaining time :eta s @ :rate images/sec - Elapsed time: :elapsed s', {
    complete: '=', head: ">", incomplete: ' ', width: 60, total: totalQuotes
  });


  app.use(express.static('../'))
  

  //start the webserver
  app.listen(params.httpServer.port, async function () {
    console.log(`Mini local server launched on port ${params.httpServer.port}`)
    console.log(`\nProcess of the ${totalQuotes} quotes starting...`)

    
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    var allPromises = []
    for ( i = 0 ; i < Object.keys(quotes).length ; i++ ) {
      for ( j = 0 ; j < quotes[Object.keys(quotes)[i]].length ; j++) {
        var res = await generateOne ( page,  Object.keys(quotes)[i] , j , bar );
      }
    }


    await browser.close();
    process.exit(0)
  })

  



  
  
})();