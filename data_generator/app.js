const fs = require('fs')

const params = {
  "sourceFile" : "../data/origin.csv",
  "outputJSON" : {
    "filename" : "../data/full_list.json"
  },
  "outputXML"  : {
    "filename" : "../data/full_list.xml",
    "parentName" : "data"
  }
}

function readCSVFile() {
  return new Promise ( (resolve, reject) => {
    const parse = require('csv-parse')
    const output = []
    
    var source = fs.readFileSync(params.sourceFile,'utf8')
  
    source = source.replace(/^\s*[\r\n]/gm,"")

    parse(source, { quoting: true, header: false })
    .on('readable', function(){
      let record
      while (record = this.read()) {
        output.push(record)
      }
    })
    
    .on('end', function(){
        resolve(output)
    })
  
  })
}



function generateXML( data ) {
  return new Promise( (resolve,reject) => {
    const  {toXML}  = require('jstoxml');
    var forXML = {};
    forXML[`${params.outputXML.parentName}`] = data
    fs.writeFileSync(params.outputXML.filename,`${toXML(forXML)}`,'utf8')
    resolve("XML done")
  })
}


function generateJSON( data ) {
  return new Promise ( (resolve, reject) => {
    var res ={ }
    for ( var i = 0 ; i < data.length ; i++) {
      //If the key does not exist, we create it.
      if (res[data[i][0]] === undefined ) res[data[i][0]] = []

      //console.log (data[i][2], data[i][3],data[i][4],data[i][5])

      res[data[i][0]].push(
        { 
          "extrait" :  data[i][1],
          "titre" : data[i][2],
          "auteur" : data[i][3],
          "traducteur" : data[i][4],
          "motClef" : data[i][5]
        } //EOJson
      ) //EOPush
    } //EOFor

    //Here, the res variable contains  the full JSON, we can store it in target file and return it.
    fs.writeFileSync(params.outputJSON.filename,JSON.stringify(res),'utf8')
    resolve(res)
  });
}

(async () => {   

    //var originData = await readCSVFile();

    var JSONData= await generateJSON(await readCSVFile())
   
    var res  = await Promise.all( [ generateXML(JSONData) ] )

    console.log(`${res}`)

})();

