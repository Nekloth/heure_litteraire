# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Changelog.md file at root
- README.md file at root
- Licence file at root
- ```images_generator``` folder to hold the nodeJS script that generates images
- .gitignore file (at root)
- Changelog.md for image generator
- Changelog.md for image generator
- ```data_generator``` folder to hold helper scripts
- ```data``` folder to hold different text reference file (JSON, XML, CSV, etc.)