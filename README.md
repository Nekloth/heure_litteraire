# Introduction


# Status

A ce jour (3 février 2019), il y a 67 citiations disponibles sur les 1440 nécessaires.

![Avancement : 4.6%](https://badgen.net/badge/Pourcentage%20d%27avancement/4.6%20%25/orange)


# Principe

**ATTENTION**: cette suite de programme et en cours, et susceptible d'être ré-organiser...

Par exemple, il y a deux scripts, un qui générère les données dans certains formats, l'autre les images. Plus les développements avancent, plus il y aurait du sens à tout mettre dans un seul script, quitte à prendre en compte des paramtètres.

DONC, il est probable que ce que ce soit revu.


## Générer les différentes sources

Tout part du fichier ```data/origin.csv```.
Ce fichier est issu d'Excel ou équivalent, en utilisant les caractéritiques suivantes:
- Pas d'en-tête
- Séparateur virgule
- Chaque valeur de champ est mis entre guillemets double (```"```)

Les colonnes (**attenetion**, l'ordre est important) sont:
- L'heure correspondant à l'extrait, au format "HHMM"
- L'extrait en lui-même (avec des ```{{NEWLINE}}``` quand un saut de ligne sera nécessaire)
- L'auteur (ou les auteurs)
- Le traducteur
- Le mot clé, celui qui sera mis en exergue (l'heure)

Si un champ n'a pas de valeur, comme le traducteur par exemple, il devra quand même y avoir les guillemets (```""```)


Ensuite, Il faut lancer le script ```node ./data_generator/app.js```.

Ce script créer, pour le momment, DEUX fichiers en sortie : ```data/full_list.json``` et ```data/full_list.xml```.

Seul le ```json``` est utilisé par le reste du process. Le ```xml``` est là pour ceux qui en auraient besoin

## Générer les différentes images

Le principe est simple: un serveur web est démarré. 

1. Pour chacune des heures dans le fichier ```json```, la page ```./data_generator/template/index.html``` est appelée avec les informations pour afficher une heure en particulier.
2. Une fois chargé, cette page est "imprimer" en image dans le dossier ```./output```
3. Fin du programme.